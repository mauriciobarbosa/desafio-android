package concrete.mauricio.javapop.features.repositorydetail;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by mauricio on 03/03/17.
 */
class TabPagerAdapter extends FragmentStatePagerAdapter {

    private Pair[] fragments;

    TabPagerAdapter(FragmentManager fm, Pair... fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments[position].key;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position].value;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    static class Pair {
        String key;
        Fragment value;

        Pair(String key, Fragment value) {
            this.key = key;
            this.value = value;
        }
    }
}
