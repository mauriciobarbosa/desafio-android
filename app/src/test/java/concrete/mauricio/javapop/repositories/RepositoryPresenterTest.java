package concrete.mauricio.javapop.repositories;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.channels.NotYetConnectedException;
import java.util.List;

import concrete.mauricio.javapop.DataUtils;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import concrete.mauricio.javapop.data.source.GitRepository;
import concrete.mauricio.javapop.features.repositories.RepositoryContract;
import concrete.mauricio.javapop.features.repositories.RepositoryPresenter;
import concrete.mauricio.javapop.utils.schedulers.ImmediateSchedulerProvider;
import rx.Observable;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

/**
 * Created by mauricio on 01/04/17.
 */
public class RepositoryPresenterTest {

    @Mock
    private AppManager mApp;

    @Mock
    private RepositoryContract.View mView;

    @Mock
    private GitRepository mRepository;

    private RepositoryPageDTO mPageDTO;

    private RepositoryPresenter mPresenter;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        when(mApp.getLanguage()).thenReturn("Java");
        when(mApp.sortBy()).thenReturn("stars");

        mPresenter = new RepositoryPresenter(mRepository, mView, new ImmediateSchedulerProvider());

        // O presenter não irá atualizar a view se ela não estiver ativa.
        when(mView.isActive()).thenReturn(true);

        mPageDTO = DataUtils.getRepositoryPage();
    }

    @Test
    public void loadRepos_showRepos() {

        when(mRepository.loadRepositories(anyInt())).thenReturn(Observable.just(mPageDTO));

        // Dado que existe um RepositoryPresenter previamente configurado com filmes no repositório
        // Quando o método loadRepositories é invocado...
        mPresenter.loadRepositories();

        // A view deve tratar os seguintes eventos...
        InOrder inOrder = inOrder(mView);
        // O indicador de carregamento é exibido.
        inOrder.verify(mView).showLoadingIndicator();
        // Os repositórios são carregados na view.
        ArgumentCaptor<List> showReposArgumentCaptor = ArgumentCaptor.forClass(List.class);
        //noinspection unchecked
        inOrder.verify(mView).showRepositories(showReposArgumentCaptor.capture(), eq(mPageDTO.page));
        // O indicador de carregamento é ocultado.
        inOrder.verify(mView).hiddenLoadingIndicator();
        // A quantidade de itens exibidos é igual a retornada pelo repositório.
        assertTrue(showReposArgumentCaptor.getValue().size() == mPageDTO.repositories.size());

    }

    @Test
    public void loadRepos_showNoRepo() {

        when(mRepository.loadRepositories(anyInt())).thenReturn(Observable.just(null));

        mPresenter.loadRepositories();

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();
        inOrder.verify(mView).showNoRepositories();
        inOrder.verify(mView).hiddenLoadingIndicator();

    }

    @Test
    public void loadRepos_showMoreRepo() {

        when(mRepository.loadRepositories(anyInt())).thenReturn(Observable.just(mPageDTO));

        mPresenter.loadMore();

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();

        ArgumentCaptor<List> showReposArgumentCaptor = ArgumentCaptor.forClass(List.class);

        //noinspection unchecked
        inOrder.verify(mView).showRepositories(showReposArgumentCaptor.capture(), anyInt());
        inOrder.verify(mView).hiddenLoadingIndicator();
        assertTrue(showReposArgumentCaptor.getValue().size() == mPageDTO.repositories.size());

    }

    @Test
    public void loadRepos_showNoConnection() {

        when(mRepository.loadRepositories(anyInt())).thenReturn(
                Observable.just(mPageDTO).doOnRequest(l -> {
                    throw new NotYetConnectedException();
                }));

        mPresenter.loadMore();

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();
        inOrder.verify(mView).showNoInternetConnection();
        inOrder.verify(mView).hiddenLoadingIndicator();

    }

    @Test
    public void refreshRepos() {

        when(mRepository.refreshRepositories()).thenReturn(Observable.just(mPageDTO));

        mPresenter.refreshRepositories();

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();

        ArgumentCaptor<List> showReposArgumentCaptor = ArgumentCaptor.forClass(List.class);
        //noinspection unchecked

        inOrder.verify(mView).showRepositories(showReposArgumentCaptor.capture(), eq(1));
        inOrder.verify(mView).hiddenLoadingIndicator();

        assertTrue(showReposArgumentCaptor.getValue().size() == mPageDTO.repositories.size());

    }

}
