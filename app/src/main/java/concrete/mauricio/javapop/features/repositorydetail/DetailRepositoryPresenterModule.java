package concrete.mauricio.javapop.features.repositorydetail;

import android.support.annotation.NonNull;

import concrete.mauricio.javapop.utils.schedulers.BaseSchedulerProvider;
import concrete.mauricio.javapop.utils.schedulers.SchedulerProvider;
import dagger.Module;
import dagger.Provides;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@Module
class DetailRepositoryPresenterModule {

    @NonNull
    private final DetailRepositoryContract.View mPullRequestView;

    public DetailRepositoryPresenterModule(@NonNull DetailRepositoryContract.View pullRequestView) {
        mPullRequestView = checkNotNull(pullRequestView);
    }

    @Provides
    DetailRepositoryContract.View providePullRequestContractView() {
        return mPullRequestView;
    }

    @Provides
    BaseSchedulerProvider provideScheduler() {
        return SchedulerProvider.getInstance();
    }
}
