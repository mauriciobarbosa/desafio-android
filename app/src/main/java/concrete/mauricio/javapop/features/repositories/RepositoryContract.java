package concrete.mauricio.javapop.features.repositories;

import android.support.annotation.NonNull;

import java.util.List;

import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.BasePresenter;
import concrete.mauricio.javapop.features.shared.BaseView;

/**
 * Created by mauricio on 29/03/17.
 */
@SuppressWarnings("ALL")
public interface RepositoryContract {

    interface Presenter extends BasePresenter {

        void refreshRepositories();

        void loadRepositories();

        void reloadRepositories();

        void loadMore();

        void openPullRequestsScreen(@NonNull RepositoryDTO repository);

    }

    interface View extends BaseView<Presenter> {

        void showRepositories(@NonNull List<RepositoryDTO> movies, int page);

        void showLoadingIndicator();

        void hiddenLoadingIndicator();

        void showNoRepositories();

        void showLoadingRepositoriesError();

        void showNoInternetConnection();

        void hasNoMoreRepositories();

        void showMovieDetailScreen(@NonNull RepositoryDTO repository);

        void restoreListState();

    }

}
