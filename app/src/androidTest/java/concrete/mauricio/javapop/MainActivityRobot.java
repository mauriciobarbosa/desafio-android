package concrete.mauricio.javapop;

import android.support.test.espresso.contrib.RecyclerViewActions;

import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.repositorydetail.DetailActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by mauricio on 11/06/17.
 */

class MainActivityRobot {

    static class Robot {

        ResultRobot selectRepository(int position) {
            onView(withId(R.id.repositories_recycler_view))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(position, click()));

            return new ResultRobot();
        }

    }

    static class ResultRobot {

        void isSuccess() {
            intended(allOf(hasComponent(DetailActivity.class.getName()),
                    hasExtra(equalTo(DetailActivity.EXTRA_REPOSITORY), any(RepositoryDTO.class))));
        }

    }

}
