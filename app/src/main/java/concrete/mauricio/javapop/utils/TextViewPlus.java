package concrete.mauricio.javapop.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import concrete.mauricio.javapop.R;

/**
 * Created by mauricio on 19/02/17.
 */
public class TextViewPlus extends android.support.v7.widget.AppCompatTextView {

    public TextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextViewPlus,
                0, 0);

        try {
            String fontName = a.getString(R.styleable.TextViewPlus_fontNameTextViewPlus);
            Font.changeFont(this, fontName);
        } finally {
            a.recycle();
        }
    }
}
