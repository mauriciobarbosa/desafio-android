package concrete.mauricio.javapop.features.repositorydetail;

import concrete.mauricio.javapop.data.source.GitRepositoryComponent;
import concrete.mauricio.javapop.utils.FragmentScoped;
import dagger.Component;

/**
 * Created by mauricio on 29/03/17.
 */
@FragmentScoped
@Component(dependencies = GitRepositoryComponent.class, modules = DetailRepositoryPresenterModule.class)
public interface DetailRepositoryComponent {

    void inject(PullRequestFragment fragment);

}
