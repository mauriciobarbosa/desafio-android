package concrete.mauricio.javapop.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;

import concrete.mauricio.javapop.data.source.GitDataSource;
import concrete.mauricio.javapop.utils.AppUtils;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@SuppressWarnings("ALL")
public class PullRequestDTO implements Serializable {

    private static final String LOG_TAG = PullRequestDTO.class.getSimpleName();

    public static final String STATE_OPEN = "open";

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("body")
    @Expose
    public String comment;

    @SerializedName("user")
    @Expose
    public UserDTO user;

    @SerializedName("html_url")
    @Expose
    public String url;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("created_at")
    @Expose
    public String dtCreate;

    public PullRequestDTO() {}

    public PullRequestDTO(@NonNull String title, @NonNull String comment, @NonNull String state) {
        this.title = checkNotNull(title);
        this.comment = checkNotNull(comment);
        this.state = checkNotNull(state);
    }

    public String getDtCreateFormatted() {
        try {
            return AppUtils.changeDateFormat(GitDataSource.SERVER_DATE_FORMAT, AppUtils.DATE_FORMAT, dtCreate);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "Error to parse dtCreated: " + e.getMessage());
            return dtCreate;
        }
    }

}
