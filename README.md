# Task

Aplicativo para consulta de repositórios Java no Github.

Autor: Mauricio Barbosa - mauriiciobarbosa@gmail.com

# Overview

A arquitetura utilizada no projeto foi o Model-View-Presenter (MVP), com os pacotes sendo divididos por funcionalidade.

Na camada de dados foi criada uma abstração denominada Repositório. Esse componente é um Singleton e tem por função gerenciar e fornecer dados aos presenters, utilizando dados de cache, fonte de dados locais e remota.

As Views são representadas por fragments, cuja única função é receber eventos disparados pelo usuário e propagá-los ao seu respectivo presenter, além de atender a comandos deste presenter.

Os Presenters são componentes intermediadores, responsáveis por receber eventos propagados pelas Views e solicitar dados ao Repositório. Além disso, os presenters são responsáveis pela lógica de negócio.

Foram criados contratos para estabelecer a responsabilidade de Views e Presenters em cada funcionalidade.

Foi adicionado ao projeto testes unitários com o objetivo de validar Presenters e Repositório. O conceito de Inversão de Controle (IoC) foi introduzido no projeto para facilitar a aplicação destes testes. Devido a simplicidade da aplicação, não foi adicionado testes instrumentados.

Obs.: Foi adicionado uma tela além da solicitada, contendo mais informações sobre o repositório selecionado. Nessa tela foi utilizada a biblioteca de databiding para "linkar" diretamente dados do modelo à view.

# Dependencies

* AndroidAnnotations - Utilizado para reduzir boilerplate code em Fragments, Activities, Aplication e SharedPreferences.
* Retrofit - Utilizado para comunicação com API RESTful.
* Picasso - Utilizado para carregamento e cache de imagens.
* Dagger2 - Utilizado para injeção de dependências nos componentes da aplicação.
* Mockito - Utilizado para mock de objetos em testes.
* Stream e Retrolambda - Utilizados para trabalhar com Streams e Lambdas em projetos Android.