package concrete.mauricio.javapop.utils;

import android.graphics.Typeface;
import android.view.View;

/**
 * Created by mauricio on 19/02/17.
 */
public class Font {

	private static final String FONT_ROBOTO_PATH = "fonts/{font}.ttf";
    private static final String DEFAULT_FONT = "Roboto-Regular";
		
	public static void changeFont(View view, String fontName) {
        Typeface typeFace = Typeface.createFromAsset(view.getContext().getAssets(), getFontPath(fontName));

        if (view instanceof TextViewPlus)
            ((TextViewPlus)view).setTypeface(typeFace);
	}

    private static String getFontPath(String fontName) {
        if(fontName == null || "".equals(fontName)) {
            fontName = DEFAULT_FONT;
        }
        return FONT_ROBOTO_PATH.replace("{font}", fontName);
    }
}