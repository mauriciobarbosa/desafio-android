package concrete.mauricio.javapop.features.repositorydetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.BaseActivity;

public class DetailActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    public static final String EXTRA_REPOSITORY = "concrete.mauricio.javapop.screens.repositorydetail.EXTRA_REPOSITORY";

    private static final short FRAG_DETAIL = 0;
    private static final short FRAG_PULL_REQUEST = 1;

    protected RepositoryDTO mSelectedRepo;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.pager)
    protected ViewPager pager;

    @BindView(R.id.tabs)
    protected TabLayout tabs;

    private String[] mSubtitles;

    private short mFragActive;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actv_detail);
        ButterKnife.bind(this);
        mSelectedRepo = (RepositoryDTO) getIntent().getSerializableExtra(EXTRA_REPOSITORY);
        init();
    }

    protected void init() {
        mSubtitles = new String[2];
        mSubtitles[FRAG_DETAIL] = mSelectedRepo.owner.login;

        configureToolbar();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_REPOSITORY, mSelectedRepo);

        DetailFragment mDetailFragment = new DetailFragment();
        PullRequestFragment mPullRequestFragment = new PullRequestFragment();

        mDetailFragment.setArguments(args);
        mPullRequestFragment.setArguments(args);

        TabPagerAdapter.Pair[] pair = new TabPagerAdapter.Pair[]{
                new TabPagerAdapter.Pair(getString(R.string.lbl_detail), mDetailFragment),
                new TabPagerAdapter.Pair(getString(R.string.lbl_pull_requests), mPullRequestFragment)
        };

        PagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager(), pair);

        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(this);
        tabs.setupWithViewPager(pager);

        mFragActive = FRAG_DETAIL;
    }

    private void configureToolbar() {
        toolbar.setTitle(mSelectedRepo.name);
        toolbar.setSubtitle(mSubtitles[FRAG_DETAIL]);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    @Override
    public void setSubtitle(@NonNull String subtitle) {
        if (!TextUtils.isEmpty(subtitle) && !TextUtils.equals(subtitle, mSubtitles[FRAG_DETAIL])) {
            mSubtitles[1] = subtitle;
            if (mFragActive == FRAG_PULL_REQUEST)
                toolbar.setSubtitle(subtitle);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (!TextUtils.isEmpty(mSubtitles[position]))
            toolbar.setSubtitle(mSubtitles[position]);
        mFragActive = (short) position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
