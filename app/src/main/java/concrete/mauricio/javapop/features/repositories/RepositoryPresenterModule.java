package concrete.mauricio.javapop.features.repositories;

import android.support.annotation.NonNull;

import concrete.mauricio.javapop.utils.schedulers.BaseSchedulerProvider;
import concrete.mauricio.javapop.utils.schedulers.SchedulerProvider;
import dagger.Module;
import dagger.Provides;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@Module
class RepositoryPresenterModule {

    @NonNull
    private final RepositoryContract.View mView;

    RepositoryPresenterModule(@NonNull RepositoryContract.View view) {
        mView = checkNotNull(view);
    }

    @Provides
    RepositoryContract.View provideRepositoryContractView() {
        return mView;
    }

    @Provides
    BaseSchedulerProvider provideScheduler() {
        return SchedulerProvider.getInstance();
    }
}
