package concrete.mauricio.javapop.features.repositorydetail;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.annimon.stream.Stream;

import java.nio.channels.NotYetConnectedException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.source.GitDataSource;
import concrete.mauricio.javapop.data.source.GitRepository;
import concrete.mauricio.javapop.utils.schedulers.BaseSchedulerProvider;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 31/03/17.
 */
public class PullRequestPresenter implements DetailRepositoryContract.Presenter {

    @NonNull
    private DetailRepositoryContract.View mView;

    @NonNull
    private GitDataSource mRepository;

    private BaseSchedulerProvider mSchedulerProvider;

    @Inject
    public PullRequestPresenter(@NonNull GitRepository repository,
                                @NonNull DetailRepositoryContract.View view,
                                @NonNull BaseSchedulerProvider schedulerProvider) {
        mView = checkNotNull(view);
        mRepository = checkNotNull(repository);
        mSchedulerProvider = checkNotNull(schedulerProvider);
    }

    @Override
    public void start() {
        loadPullRequests();
    }

    @Override
    public void loadPullRequests() {
        mView.showLoadingIndicator();

        mRepository
                .loadPullRequests(checkNotNull(mView.getRepository()))
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        this::onConnectionSuccess,
                        // onError
                        this::onConnectionError,
                        // onComplete
                        () -> mView.hiddenLoadingIndicator()
                );

    }

    @Override
    public void refreshPullRequests() {
        mView.showLoadingIndicator();

        mRepository
                .refreshPullRequests(checkNotNull(mView.getRepository()))
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        this::onConnectionSuccess,
                        // onError
                        this::onConnectionError,
                        // onComplete
                        () -> mView.hiddenLoadingIndicator()
                );

    }

    @NonNull
    @Override
    public Map<Integer, Integer> getStatistics(@NonNull List<PullRequestDTO> pullRequests) {

//        Observable.from(pullRequests)
//                .filter(p -> PullRequestDTO.STATE_OPEN.equals(p.state))
//                .toList()
//                .map(list -> {
//                    Map<Integer, Integer> statistics = new ArrayMap<>();
//                    statistics.put(DetailRepositoryContract.PULL_REQUEST_OPENED, list.size());
//                    statistics.put(DetailRepositoryContract.PULL_REQUEST_CLOSED, pullRequests.size() - list.size());
//                    return statistics;
//                })
//                .subscribeOn(mSchedulerProvider.computation())
//                .observeOn(mSchedulerProvider.ui())
//                .subscribe(statistics -> {
//                    if (mView.isActive()) mView.updateStatistics(statistics);
//                });

        int opened = (int) Stream.of(pullRequests)
                .filter(p -> PullRequestDTO.STATE_OPEN.equals(p.state))
                .count();
        int closed = pullRequests.size() - opened;

        Map<Integer, Integer> statistics = new ArrayMap<>();
        statistics.put(DetailRepositoryContract.PULL_REQUEST_OPENED, opened);
        statistics.put(DetailRepositoryContract.PULL_REQUEST_CLOSED, closed);

        return statistics;
    }

    @Override
    public void openPullRequestDetail(@NonNull PullRequestDTO pullRequest) {
        mView.openLinkOnBrowser(pullRequest.url);
    }

    private void onConnectionSuccess(List<PullRequestDTO> pullRequests) {
        if (mView.isActive()) {
            if (pullRequests != null && pullRequests.size() > 0)
                mView.showPullRequests(pullRequests);
            else
                mView.showNoPullRequests();
        }
    }

    private void onConnectionError(Throwable error) {
        if (mView.isActive()) {
            if (error instanceof NotYetConnectedException) mView.showNoInternetConnection();
            else mView.showLoadingPullRequestsError();
            mView.hiddenLoadingIndicator();
        }
    }

}
