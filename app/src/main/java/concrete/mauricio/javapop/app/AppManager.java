package concrete.mauricio.javapop.app;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.data.source.DaggerGitRepositoryComponent;
import concrete.mauricio.javapop.data.source.GitRepositoryComponent;

/**
 * Created by mauricio on 23/03/17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class AppManager extends Application {

    private GitRepositoryComponent mRepositoryComponent;
    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context) {
        AppManager application = (AppManager) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) return;

        refWatcher = LeakCanary.install(this);

        mRepositoryComponent = DaggerGitRepositoryComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public String getLanguage() {
        return getSharedPreferences("UserPrefs", 0).getString("language", getString(R.string.pref_language_default_value));
    }

    public String sortBy() {
        return getSharedPreferences("UserPrefs", 0).getString("sortBy", getString(R.string.pref_sort_by_default_value));
    }

    @NonNull
    public GitRepositoryComponent getRepositoryComponent() {
        return mRepositoryComponent;
    }

}
