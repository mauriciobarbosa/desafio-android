package concrete.mauricio.javapop.features.repositorydetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.BaseActivity;
import concrete.mauricio.javapop.utils.AppUtils;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 31/03/17.
 */
public class PullRequestFragment extends Fragment
        implements DetailRepositoryContract.View,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.pull_requests_recycler_view)
    protected RecyclerView mRepositoriesView;

    @BindView(R.id.empty_view)
    protected LinearLayout emptyView;

    @BindView(R.id.swRefresh)
    protected SwipeRefreshLayout swRefresh;

    private PullRequestAdapter mAdapter;

    protected RepositoryDTO mSelectedRepo;

    @Inject
    protected PullRequestPresenter mPresenter;

    protected Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_list_pull_request, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        config();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppUtils.watchObject(getActivity(), this);
    }

    private void config() {

        mSelectedRepo = (RepositoryDTO) getArguments().getSerializable(DetailActivity.EXTRA_REPOSITORY);
        AppManager app = (AppManager) getActivity().getApplication();

        DaggerDetailRepositoryComponent.builder()
                .detailRepositoryPresenterModule(new DetailRepositoryPresenterModule(this))
                .gitRepositoryComponent(app.getRepositoryComponent())
                .build()
                .inject(this);

        swRefresh.setColorSchemeResources(R.color.colorAccent,
                R.color.colorPrimary, R.color.colorPrimaryDark);
        swRefresh.setOnRefreshListener(this);

        mAdapter = new PullRequestAdapter(this);

        mRepositoriesView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRepositoriesView.setLayoutManager(layoutManager);

        mRepositoriesView.addItemDecoration(new DividerItemDecoration(getContext(),
                layoutManager.getOrientation()));

        init();
    }

    @Override
    public void init() {
        mPresenter.start();
    }

    @Override
    public void setPresenter(@NonNull DetailRepositoryContract.Presenter presenter) {
        // do nothing
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @NonNull
    @Override
    public RepositoryDTO getRepository() {
        return mSelectedRepo;
    }

    @Override
    public void showPullRequests(@NonNull List<PullRequestDTO> pullRequests) {
        mAdapter.setItems(checkNotNull(pullRequests));
        mRepositoriesView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        showStatistics(mPresenter.getStatistics(pullRequests));
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void showStatistics(@NonNull Map<Integer, Integer> statistics) {
        int opened = statistics.get(DetailRepositoryContract.PULL_REQUEST_OPENED);
        int closed = statistics.get(DetailRepositoryContract.PULL_REQUEST_CLOSED);
        String subtitle = getString(R.string.repo_statistics, opened, closed);
        ((BaseActivity) getActivity()).setSubtitle(subtitle);
    }

    @Override
    public void showLoadingIndicator() {
        swRefresh.setRefreshing(true);
    }

    @Override
    public void hiddenLoadingIndicator() {
        swRefresh.setRefreshing(false);
    }

    @Override
    public void showNoPullRequests() {
        mRepositoriesView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingPullRequestsError() {
        //noinspection ConstantConditions
        Snackbar.make(getView(), getString(R.string.msg_error_on_load_images), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.lbl_retry), this::retry)
                .show();
    }

    @SuppressWarnings("UnusedParameters")
    private void retry(View view) {
        mPresenter.loadPullRequests();
    }

    @Override
    public void showNoInternetConnection() {
        if (mAdapter == null || mAdapter.getItemCount() == 0)
            emptyView.setVisibility(View.VISIBLE);
        //noinspection ConstantConditions
        Snackbar.make(getView(), getString(R.string.msg_has_no_connection), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.lbl_retry), this::retry)
                .show();
    }

    @Override
    public void openLinkOnBrowser(@NonNull String url) {
        Intent action = new Intent(Intent.ACTION_VIEW);
        action.setData(Uri.parse(url));
        startActivity(action);
    }

    @Override
    public void onRefresh() {
        mAdapter.clear();
        mPresenter.refreshPullRequests();
    }

    @Override
    public void onClick(View view) {
        int position = mRepositoriesView.getChildLayoutPosition(view);
        PullRequestDTO item = mAdapter.getItems().get(position);
        mPresenter.openPullRequestDetail(item);
    }

}
