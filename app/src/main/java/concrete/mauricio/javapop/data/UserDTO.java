package concrete.mauricio.javapop.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mauricio on 29/03/17.
 */
@SuppressWarnings("ALL")
public class UserDTO implements Serializable {

    @SerializedName("login")
    @Expose
    public String login;

    @SerializedName("avatar_url")
    @Expose
    public String avatar;

}
