package concrete.mauricio.javapop.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;

import concrete.mauricio.javapop.data.source.GitDataSource;
import concrete.mauricio.javapop.utils.AppUtils;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@SuppressWarnings("ALL")
public class RepositoryDTO implements Serializable {

    private static final String LOG_TAG = RepositoryDTO.class.getSimpleName();

    @SerializedName("id")
    @Expose
    public Integer remoteId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("full_name")
    @Expose
    public String fullName;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("html_url")
    @Expose
    public String url;

    @SerializedName("owner")
    @Expose
    public UserDTO owner;

    @SerializedName("stargazers_count")
    @Expose
    public Integer stars;

    @SerializedName("forks")
    @Expose
    public Integer forks;

    @SerializedName("created_at")
    @Expose
    public String dtCreate;

    @SerializedName("updated_at")
    @Expose
    public String dtLastUpdate;

    public RepositoryDTO() {}

    public RepositoryDTO(@NonNull String name, @NonNull String description, Integer stars, Integer forks) {
        this.name = checkNotNull(name);
        this.description = checkNotNull(description);
        this.stars = stars;
        this.forks = forks;
    }

    public String getDtCreateFormatted() {
        try {
            return AppUtils.changeDateFormat(GitDataSource.SERVER_DATE_FORMAT, AppUtils.DATE_FORMAT, dtCreate);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "Error to parse dtCreated: " + e.getMessage());
            return dtCreate;
        }
    }

    public String getDtLastUpdateFormatted() {
        try {
            return AppUtils.changeDateFormat(GitDataSource.SERVER_DATE_FORMAT, AppUtils.DATE_FORMAT, dtLastUpdate);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "Error to parse dtLastUpdate: " + e.getMessage());
            return dtLastUpdate;
        }
    }
}
