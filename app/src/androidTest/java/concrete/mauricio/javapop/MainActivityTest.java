package concrete.mauricio.javapop;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.intent.rule.IntentsTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import concrete.mauricio.javapop.features.repositories.MainActivity;
import concrete.mauricio.javapop.features.repositories.RepositoryPresenter;

/**
 * Created by mauricio on 11/06/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> mIntentsRule = new IntentsTestRule<>(MainActivity.class);

    @Mock
    public RepositoryPresenter mPresenter;

    /**
     * Prepare your test fixture for this test. In this case we register an IdlingResources with
     * Espresso. IdlingResource resource is a great way to tell Espresso when your app is in an
     * idle state. This helps Espresso to synchronize your test actions, which makes tests significantly
     * more reliable.
     */
    @Before
    public void registerIdlingResource() {
        Espresso.registerIdlingResources(
                mIntentsRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void onClickRepository_launchesDetailScreen() {
        MainActivityRobot.Robot robot = new MainActivityRobot.Robot();
        robot.selectRepository(10).isSuccess();
    }

}
