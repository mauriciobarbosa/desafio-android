package concrete.mauricio.javapop.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import concrete.mauricio.javapop.app.AppManager;

/**
 * Created by mauricio on 29/03/17.
 */
public final class AppUtils {

    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasInternetConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @SuppressWarnings("SameParameterValue")
    public static String changeDateFormat(String oldDateFormat, String newDateFormat, String strDate) throws ParseException {
        SimpleDateFormat oldDateFormatter = new SimpleDateFormat(oldDateFormat, Locale.ENGLISH);
        SimpleDateFormat newDateFormatter = new SimpleDateFormat(newDateFormat, Locale.ENGLISH);
        Date date = oldDateFormatter.parse(strDate);
        return newDateFormatter.format(date);
    }

    @SuppressWarnings("unchecked")
    public static void goToNextActivity(@NonNull AppCompatActivity source, @NonNull Intent intent, @Nullable ActivityOptionsCompat options) {
        if (options == null)
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(source);

        ActivityCompat.startActivity(source, intent, options.toBundle());
    }

    public static void watchObject(Context context, Object object) {
        AppManager.getRefWatcher(context).watch(object);
    }

}
