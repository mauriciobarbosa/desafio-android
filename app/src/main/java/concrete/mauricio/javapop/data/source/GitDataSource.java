package concrete.mauricio.javapop.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import rx.Observable;

/**
 * Created by mauricio on 29/03/17.
 */
public interface GitDataSource {

    String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss'Z'";

    Observable< RepositoryPageDTO > loadRepositories(int page);

    Observable<RepositoryPageDTO> reloadRepositories();

    Observable<RepositoryPageDTO> refreshRepositories();

    Observable<List<PullRequestDTO>> loadPullRequests(@NonNull RepositoryDTO repository);

    Observable<List<PullRequestDTO>> refreshPullRequests(@NonNull RepositoryDTO repository);

}
