package concrete.mauricio.javapop.features.shared;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;

import java.io.Serializable;

/**
 * Created by mauricio on 29/03/17.
 */
public abstract class BaseActivity extends AppCompatActivity {

    public abstract void setSubtitle(@NonNull String subtitle);

    public interface Callback {
        void onItemSelected(Serializable item, ActivityOptionsCompat options);
    }

}
