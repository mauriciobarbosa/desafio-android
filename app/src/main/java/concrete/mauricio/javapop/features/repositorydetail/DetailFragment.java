package concrete.mauricio.javapop.features.repositorydetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.databinding.FragDetailRepositoryBinding;
import concrete.mauricio.javapop.utils.AppUtils;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mauricio on 31/03/17.
 */
public class DetailFragment extends Fragment {

    @BindView(R.id.owner_image_view)
    protected CircleImageView ownerImage;

    protected Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_detail_repository, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fillRepositoryInfo();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppUtils.watchObject(getActivity(), this);
    }

    @SuppressWarnings("ConstantConditions")
    private void fillRepositoryInfo() {
        RepositoryDTO selectedRepo = (RepositoryDTO) getArguments().getSerializable(DetailActivity.EXTRA_REPOSITORY);
        FragDetailRepositoryBinding mViewDataBinding = FragDetailRepositoryBinding.bind(getView());
        mViewDataBinding.setRepository(selectedRepo);
        Picasso.with(getContext())
                .load(selectedRepo.owner.avatar)
                .placeholder(R.drawable.ic_owner)
                .error(R.drawable.ic_owner)
                .into(ownerImage);
    }

}
