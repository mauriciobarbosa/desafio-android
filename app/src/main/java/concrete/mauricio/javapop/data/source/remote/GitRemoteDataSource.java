package concrete.mauricio.javapop.data.source.remote;

import android.support.annotation.NonNull;

import java.nio.channels.NotYetConnectedException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import concrete.mauricio.javapop.BuildConfig;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import concrete.mauricio.javapop.data.source.GitDataSource;
import concrete.mauricio.javapop.utils.AppUtils;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@Singleton
public class GitRemoteDataSource implements GitDataSource {

    private static final String QUERY_PARAM = "q";
    private static final String LANGUAGE_PARAM = "language";
    private static final String PAGE_PARAM = "page";
    private static final String SORT_PARAM = "sort";

    @NonNull
    private AppManager mApp;

    private GithubAPI githubAPI;

    @Inject
    public GitRemoteDataSource(@NonNull AppManager context) {
        mApp = checkNotNull(context);
    }

    private void config() {
        if (githubAPI == null) {
            githubAPI = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(GithubAPI.class);
        }
    }

    @Override
    public Observable<RepositoryPageDTO> loadRepositories(int page) {

        config();

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put(QUERY_PARAM, LANGUAGE_PARAM + ":" + mApp.getLanguage());
        queryParams.put(PAGE_PARAM, String.valueOf(page));
        queryParams.put(SORT_PARAM, mApp.sortBy());

        return githubAPI.getRepositories(queryParams)
                .doOnRequest(l -> {
                    if (!AppUtils.hasInternetConnection(mApp)) throw new NotYetConnectedException();
                });

    }

    @Override
    public Observable<RepositoryPageDTO> refreshRepositories() {
        return loadRepositories(1);
    }

    @Override
    public Observable<List<PullRequestDTO>> loadPullRequests(@NonNull RepositoryDTO repository) {

        config();

        return githubAPI.getPullRequests(repository.owner.login, repository.name)
                .doOnRequest(l -> {
                    if (!AppUtils.hasInternetConnection(mApp)) throw new NotYetConnectedException();
                });
    }

    @Override
    public Observable<List<PullRequestDTO>> refreshPullRequests(@NonNull RepositoryDTO repository) {
        return loadPullRequests(repository);
    }

    @Override
    public Observable<RepositoryPageDTO> reloadRepositories() {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
