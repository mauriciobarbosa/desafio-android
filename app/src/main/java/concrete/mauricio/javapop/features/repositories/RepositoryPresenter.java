package concrete.mauricio.javapop.features.repositories;

import android.support.annotation.NonNull;

import java.nio.channels.NotYetConnectedException;

import javax.inject.Inject;

import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import concrete.mauricio.javapop.data.source.GitDataSource;
import concrete.mauricio.javapop.data.source.GitRepository;
import concrete.mauricio.javapop.utils.EspressoIdlingResource;
import concrete.mauricio.javapop.utils.schedulers.BaseSchedulerProvider;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
public class RepositoryPresenter implements RepositoryContract.Presenter {

    @NonNull
    private RepositoryContract.View mView;

    @NonNull
    private GitDataSource mRepository;

    private BaseSchedulerProvider mSchedulerProvider;

    private int mPage;

    @Inject
    public RepositoryPresenter(@NonNull GitRepository repository,
                               @NonNull RepositoryContract.View view,
                               @NonNull BaseSchedulerProvider schedulerProvider) {
        mView = checkNotNull(view);
        mRepository = checkNotNull(repository);
        mSchedulerProvider = checkNotNull(schedulerProvider);
    }

    @Override
    public void start() {
        loadRepositories();
    }

    @Override
    public void refreshRepositories() {
        mPage = 1;

        mView.showLoadingIndicator();

        mRepository
                .refreshRepositories()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        this::onConnectionSuccess,
                        // onError
                        this::onConnectionError,
                        // onComplete
                        () -> mView.hiddenLoadingIndicator()
                );

    }

    @Override
    public void loadRepositories() {
        mView.showLoadingIndicator();
        loadRepositories(mPage = 1);
    }

    @Override
    public void loadMore() {
        mView.showLoadingIndicator();
        loadRepositories(++mPage);
    }

    private void loadRepositories(int page) {

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment(); // App is busy until further notice

        mRepository
                .loadRepositories(page)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doOnTerminate(EspressoIdlingResource::decrement)
                .subscribe(
                        // onNext
                        this::onConnectionSuccess,
                        // onError
                        this::onConnectionError,
                        // onComplete
                        () -> mView.hiddenLoadingIndicator()
                );
    }

    @Override
    public void reloadRepositories() {
        mRepository
                .reloadRepositories()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        pageDTO -> {
                            mPage = pageDTO.page;
                            onConnectionSuccess(pageDTO);
                            if (mView.isActive()) mView.restoreListState();
                        },
                        // onError
                        this::onConnectionError,
                        // onComplete
                        () -> mView.hiddenLoadingIndicator()
                );
    }

    private void onConnectionSuccess(RepositoryPageDTO pageDTO) {
        if (mView.isActive()) {
            if (pageDTO != null && pageDTO.repositories != null && pageDTO.repositories.size() > 0)
                mView.showRepositories(pageDTO.repositories, mPage);
            else if (mPage > 1)
                mView.hasNoMoreRepositories();
            else
                mView.showNoRepositories();
        }
    }

    private void onConnectionError(Throwable error) {
        if (mView.isActive()) {
            if (error instanceof NotYetConnectedException) mView.showNoInternetConnection();
            else mView.showLoadingRepositoriesError();
            mView.hiddenLoadingIndicator();
        }
    }

    @Override
    public void openPullRequestsScreen(@NonNull RepositoryDTO repository) {
        mView.showMovieDetailScreen(checkNotNull(repository));
    }

}
