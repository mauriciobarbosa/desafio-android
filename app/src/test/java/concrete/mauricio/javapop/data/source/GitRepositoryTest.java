package concrete.mauricio.javapop.data.source;

import android.content.Context;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import concrete.mauricio.javapop.DataUtils;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import rx.Observable;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mauricio on 01/04/17.
 */

public class GitRepositoryTest {

    @Mock
    private GitDataSource mLocalDataSource;

    @Mock
    private GitDataSource mRemoteDataSource;

    @Mock
    private Context mContext;

    private GitRepository mRepository;

    private RepositoryDTO mRepo;
    private RepositoryPageDTO mPageDTO;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        mRepository = new GitRepository(mLocalDataSource, mRemoteDataSource);

        mRepo = DataUtils.getRepository();
        mPageDTO = DataUtils.getRepositoryPage();

        when(mRemoteDataSource.loadRepositories(anyInt())).thenReturn(Observable.just(mPageDTO));
        when(mRemoteDataSource.loadPullRequests(eq(mRepo))).thenReturn(Observable.just(DataUtils.getPullRequests()));

    }

    @Test
    public void getRepos_repositoryCachesAfterFirstApiCall() {
        // Dado um Captor para capturar callbacks
        // Quando duas requisições para capturar a mesma página de repositórios são realizadas ao repositório...
        twoLoadRepoCallsToRepository(1, 1);

        // Então os itens são requisitados apenas uma vez a API.
        verify(mRemoteDataSource, times(1)).loadRepositories(eq(1));

    }

    @Test
    public void getRepos_requestTwoPages() {

        twoLoadRepoCallsToRepository(1, 2);

        verify(mRemoteDataSource, times(2)).loadRepositories(anyInt());

    }

    @Test
    public void getRepos_requestDataWithDirtyCache() {

        mRepository.loadRepositories(1);

        mRepository.refreshRepositories();

        verify(mRemoteDataSource, times(2)).loadRepositories(anyInt());

    }

    @Test
    public void getRepos_reloadAllReposInMemoryCache() {

        twoLoadRepoCallsToRepository(1, 2);

        mRepository.reloadRepositories().subscribe(result -> {
            assertTrue(result.repositories.size() == mPageDTO.repositories.size() * 2);
        });

    }

    @Test
    public void getPullRequests_repositoryCachesAfterFirstApiCall() {

        twoLoadPullRequestCallsToRepository();

        verify(mRemoteDataSource, times(1)).loadPullRequests(eq(mRepo));

    }

    @Test
    public void getPullRequests_requestDataWithDirtyCache() {

        mRepository.loadPullRequests(mRepo);

        verify(mRemoteDataSource).loadPullRequests(eq(mRepo));

        mRepository.refreshPullRequests(mRepo);

        verify(mRemoteDataSource, times(2)).loadPullRequests(eq(mRepo));

    }

    private void twoLoadRepoCallsToRepository(int firstPage, int secondPage) {

        mRepository.loadRepositories(firstPage).subscribe();

        mRepository.loadRepositories(secondPage).subscribe();

    }

    private void twoLoadPullRequestCallsToRepository() {

        mRepository.loadPullRequests(mRepo).subscribe();

        mRepository.loadPullRequests(mRepo).subscribe();

    }

}
