package concrete.mauricio.javapop.features.repositorydetail;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Map;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.BasePresenter;
import concrete.mauricio.javapop.features.shared.BaseView;

/**
 * Created by mauricio on 31/03/17.
 */

public interface DetailRepositoryContract {

    int PULL_REQUEST_OPENED = 1;
    int PULL_REQUEST_CLOSED = 2;

    interface Presenter extends BasePresenter {

        void loadPullRequests();

        void refreshPullRequests();

        void openPullRequestDetail(@NonNull PullRequestDTO pullRequest);

        @NonNull
        Map<Integer, Integer> getStatistics(@NonNull List<PullRequestDTO> pullRequests);

    }

    interface View extends BaseView<Presenter> {

        void showPullRequests(@NonNull List<PullRequestDTO> pullRequests);

        void showLoadingIndicator();

        void hiddenLoadingIndicator();

        void showNoPullRequests();

        void showLoadingPullRequestsError();

        void showNoInternetConnection();

        void openLinkOnBrowser(@NonNull String url);

        void showStatistics(@NonNull Map<Integer, Integer> statistics);

        @NonNull
        RepositoryDTO getRepository();
    }

}
