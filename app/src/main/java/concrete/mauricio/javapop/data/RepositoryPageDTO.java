package concrete.mauricio.javapop.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mauricio on 29/03/17.
 */
public class RepositoryPageDTO implements Serializable {

    public int page;

    @SerializedName("items")
    @Expose
    public List<RepositoryDTO> repositories;

}
