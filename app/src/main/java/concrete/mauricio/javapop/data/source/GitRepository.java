package concrete.mauricio.javapop.data.source;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.annimon.stream.Stream;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import rx.Observable;

import static com.annimon.stream.Collectors.toList;
import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@Singleton
public class GitRepository implements GitDataSource {

    private static final int MAX_SIZE = 25;

    @NonNull
    private final GitDataSource mLocalDataSource;

    @NonNull
    private final GitDataSource mRemoteDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private Map<Integer, List<RepositoryDTO>> mCachedRepositories;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private Map<Integer, List<PullRequestDTO>> mCachedPullRequests;

    @Inject
    public GitRepository(@Local GitDataSource localDataSource,
                         @Remote GitDataSource remoteDataSource) {
        mLocalDataSource = checkNotNull(localDataSource);
        mRemoteDataSource = checkNotNull(remoteDataSource);
        mCachedRepositories = new ArrayMap<>(MAX_SIZE);
        mCachedPullRequests = new ArrayMap<>(MAX_SIZE);
    }

    @Override
    public Observable<RepositoryPageDTO> loadRepositories(int page) {

        if (mCachedRepositories.containsKey(page)) {
            RepositoryPageDTO pageDTO = new RepositoryPageDTO();
            pageDTO.page = page;
            pageDTO.repositories = mCachedRepositories.get(page);
            return Observable.just(pageDTO);
        }

        return getAndSaveRemoteRepositories(page);
    }

    private Observable<RepositoryPageDTO> getAndSaveRemoteRepositories(int page) {
        return mRemoteDataSource
                .loadRepositories(page)
                .flatMap(repositoryPageDTO ->
                        Observable.just(repositoryPageDTO).doOnNext(pageDTO -> {
                            if (pageDTO != null)
                                mCachedRepositories.put(page, pageDTO.repositories);
                        }));
    }

    @Override
    public Observable<RepositoryPageDTO> refreshRepositories() {
        clearCache();
        return getAndSaveRemoteRepositories(1);
    }

    @Override
    public Observable<List<PullRequestDTO>> loadPullRequests(@NonNull RepositoryDTO repository) {

        checkNotNull(repository);

        if (mCachedPullRequests.containsKey(repository.remoteId)) {
            return Observable.from(mCachedPullRequests.get(repository.remoteId)).toList();
        }

        return getAndSaveRemotePullRequests(repository);
    }

    private Observable<List<PullRequestDTO>> getAndSaveRemotePullRequests(RepositoryDTO repository) {
        return mRemoteDataSource
                .loadPullRequests(repository)
                .flatMap(pullRequests -> Observable.just(pullRequests).doOnNext(
                        pullRequestList -> {
                            //noinspection unchecked
                            mCachedPullRequests.put(repository.remoteId, pullRequestList);
                        }));

    }

    @Override
    public Observable<List<PullRequestDTO>> refreshPullRequests(@NonNull RepositoryDTO repository) {

        checkNotNull(repository);

        mCachedPullRequests.clear();

        return getAndSaveRemotePullRequests(repository);
    }

    @Override
    public Observable<RepositoryPageDTO> reloadRepositories() {
        if (mCachedRepositories.size() > 0) {
            RepositoryPageDTO pageDTO = new RepositoryPageDTO();
            pageDTO.page = mCachedRepositories.size();
            pageDTO.repositories = Stream.of(mCachedRepositories.values())
                    .flatMap(Stream::of)
                    .collect(toList());
            return Observable.just(pageDTO);
        }
        return getAndSaveRemoteRepositories(1);
    }

    private void clearCache() {
        mCachedRepositories.clear();
        mCachedPullRequests.clear();
    }

}
