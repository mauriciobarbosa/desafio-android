package concrete.mauricio.javapop;

import java.util.Arrays;
import java.util.List;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;

/**
 * Created by mauricio on 01/04/17.
 */

public final class DataUtils {

    private static RepositoryPageDTO sPageDTO;

    private static List<PullRequestDTO> sPullRequests;

    private static RepositoryDTO sRepo;

    static {
        sPageDTO = new RepositoryPageDTO();
        sPageDTO.page = 1;
        sPageDTO.repositories = Arrays.asList(
                new RepositoryDTO("RxJava", "Reactive Extensions for the JVM", 22910, 4046),
                new RepositoryDTO("elasticsearch", "Open Source, Distributed, RESTful Search Engine", 21782, 7237),
                new RepositoryDTO("retrofit", "Type-safe HTTP client for Android and Java by Square, Inc", 20165, 4173)
        );
        sRepo = sPageDTO.repositories.get(0);
        sPullRequests = Arrays.asList(
                new PullRequestDTO("More nullability annotations", "Added nullability annotations...", PullRequestDTO.STATE_OPEN),
                new PullRequestDTO("Defer creation of the TimeoutException when using...", "Use a defer instead of simply...", PullRequestDTO.STATE_OPEN),
                new PullRequestDTO("fix async single error report (bug #5237)", "Targets issue #5237...", PullRequestDTO.STATE_OPEN)
        );
    }

    public static RepositoryPageDTO getRepositoryPage() {
        return sPageDTO;
    }

    public static RepositoryDTO getRepository() {
        return sRepo;
    }

    public static List<PullRequestDTO> getPullRequests() {
        return sPullRequests;
    }

}
