package concrete.mauricio.javapop.features.repositories;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.leakcanary.RefWatcher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.BaseActivity;
import concrete.mauricio.javapop.utils.AppUtils;
import concrete.mauricio.javapop.utils.EndlessRecyclerViewScrollListener;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
public class RepositoryFragment extends Fragment
        implements RepositoryContract.View,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String RELOAD = "concrete.mauricio.javapop.features.repositories.RepositoryFragment.RELOAD";
    private static final String RECYCLER_VIEW_STATE = "concrete.mauricio.javapop.features.repositories.RECYCLER_VIEW_STATE";

    @BindView(R.id.repositories_recycler_view)
    protected RecyclerView mRepositoriesView;

    @BindView(R.id.empty_view)
    protected LinearLayout emptyView;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.swRefresh)
    protected SwipeRefreshLayout swRefresh;

    protected EndlessRecyclerViewScrollListener scrollListener;

    protected RepositoryAdapter mAdapter;

    private BaseActivity.Callback listener;

    protected RepositoryContract.Presenter mPresenter;

    protected Unbinder unbinder;

    private boolean isAbleToReload;

    private Parcelable mRecyclerViewState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            mPresenter.refreshRepositories();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_list_repository, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            isAbleToReload = savedInstanceState.getBoolean(RELOAD, false);
            mRecyclerViewState = savedInstanceState.getParcelable(RECYCLER_VIEW_STATE);
        }

        configure();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(RELOAD, true);
        outState.putParcelable(RECYCLER_VIEW_STATE, mRepositoriesView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppUtils.watchObject(getActivity(), this);
    }

    protected void configure() {

        toolbar.setNavigationIcon(R.drawable.ic_github);

        swRefresh.setColorSchemeResources(R.color.colorAccent,
                R.color.colorPrimary, R.color.colorPrimaryDark);
        swRefresh.setOnRefreshListener(this);

        BaseActivity activity = (BaseActivity) getActivity();

        if (!(activity instanceof BaseActivity.Callback))
            throw new IllegalStateException("Activity must implement MovieListFragment.Callback");

        listener = (BaseActivity.Callback) activity;
        activity.setSupportActionBar(toolbar);

        mAdapter = new RepositoryAdapter(this);

        mRepositoriesView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRepositoriesView.setLayoutManager(layoutManager);

        mRepositoriesView.addItemDecoration(new DividerItemDecoration(getContext(),
                    layoutManager.getOrientation()));

        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mPresenter.loadMore();
            }
        };

        mRepositoriesView.addOnScrollListener(scrollListener);

        init();

    }

    @Override
    public void init() {
        if (isAbleToReload) mPresenter.reloadRepositories();
        else mPresenter.loadRepositories();
    }

    @Override
    public void setPresenter(@NonNull RepositoryContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showRepositories(@NonNull List<RepositoryDTO> movies, int page) {
        if (page == 1) {
            scrollListener.resetState();
            mAdapter.clear();
        }

        mAdapter.setItems(checkNotNull(movies));
        mRepositoriesView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void restoreListState() {
        mRepositoriesView.getLayoutManager().onRestoreInstanceState(mRecyclerViewState);
    }

    @Override
    public void showLoadingIndicator() {
        swRefresh.setRefreshing(true);
    }

    @Override
    public void hiddenLoadingIndicator() {
        swRefresh.setRefreshing(false);
    }

    @Override
    public void showNoRepositories() {
        mRepositoriesView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingRepositoriesError() {
        //noinspection ConstantConditions
        Snackbar.make(getView(), getString(R.string.msg_error_on_load_images), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.lbl_retry), this::retry)
                .show();
    }

    @Override
    public void showNoInternetConnection() {
        if (mAdapter == null || mAdapter.getItemCount() == 0)
            emptyView.setVisibility(View.VISIBLE);
        //noinspection ConstantConditions
        Snackbar.make(getView(), getString(R.string.msg_has_no_connection), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.lbl_retry), this::retry)
                .show();
    }

    @SuppressWarnings("UnusedParameters")
    private void retry(View view) {
        if (mAdapter != null && mAdapter.getItemCount() > 0)
            mPresenter.loadMore();
        else
            mPresenter.refreshRepositories();
    }

    @Override
    public void hasNoMoreRepositories() {
        //noinspection ConstantConditions
        Snackbar.make(getView(), getString(R.string.msg_has_no_more_data), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        mPresenter.refreshRepositories();
    }

    @Override
    public void onClick(View view) {
        int position = mRepositoriesView.getChildLayoutPosition(view);
        RepositoryDTO item = mAdapter.getItems().get(position);
        mPresenter.openPullRequestsScreen(item);
    }

    @Override
    public void showMovieDetailScreen(@NonNull RepositoryDTO repository) {
        listener.onItemSelected(repository, null);
    }

}
