package concrete.mauricio.javapop.features.repositorydetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.features.shared.RecyclerViewBaseAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 20/02/17.
 */
class PullRequestAdapter extends RecyclerViewBaseAdapter<PullRequestDTO, PullRequestAdapter.PullRequestViewHolder> {

    @NonNull
    private View.OnClickListener mListener;

    PullRequestAdapter(@NonNull View.OnClickListener listener) {
        mListener = checkNotNull(listener);
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pull_request, null);
        view.setOnClickListener(mListener);
        return new PullRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int position) {
        holder.bind(getItems().get(position));
    }

    static class PullRequestViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image_view)
        CircleImageView userImage;

        @BindView(R.id.user_name_text_view)
        TextView userName;

        @BindView(R.id.pull_request_title_text_view)
        TextView pullRequestTitle;

        @BindView(R.id.pull_request_desc_text_view)
        TextView pullRequestDescription;

        @BindView(R.id.pull_request_date_text_view)
        TextView date;

        PullRequestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PullRequestDTO pullRequest) {
            Picasso.with(itemView.getContext())
                    .load(pullRequest.user.avatar)
                    .placeholder(R.drawable.ic_owner)
                    .error(R.drawable.ic_owner)
                    .into(userImage);

            userName.setText(pullRequest.user.login);
            pullRequestTitle.setText(pullRequest.title);
            pullRequestDescription.setText(pullRequest.comment);
            date.setText(pullRequest.getDtCreateFormatted());
        }

    }

}
