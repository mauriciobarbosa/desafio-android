package concrete.mauricio.javapop.app;

import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 28/03/17.
 */
@Module
public class AppModule {

    @NonNull
    private final AppManager mContext;

    AppModule(@NonNull AppManager mContext) {
        this.mContext = checkNotNull(mContext);
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    AppManager provideAppManager() {
        return mContext;
    }
}
