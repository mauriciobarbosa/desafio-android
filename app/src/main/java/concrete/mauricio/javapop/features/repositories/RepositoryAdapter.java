package concrete.mauricio.javapop.features.repositories;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.features.shared.RecyclerViewBaseAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 20/02/17.
 */
class RepositoryAdapter extends RecyclerViewBaseAdapter<RepositoryDTO, RepositoryAdapter.RepositoryViewHolder> {

    @NonNull
    private View.OnClickListener mListener;

    RepositoryAdapter(@NonNull View.OnClickListener listener) {
        mListener = checkNotNull(listener);
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, null);
        view.setOnClickListener(mListener);
        return new RepositoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        holder.bind(getItems().get(position));
    }

    static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.owner_image_view)
        CircleImageView ownerImage;

        @BindView(R.id.owner_name_text_view)
        TextView ownerName;

        @BindView(R.id.repository_name_text_view)
        TextView repositoryName;

        @BindView(R.id.repository_desc_text_view)
        TextView repositoryDescription;

        @BindView(R.id.fork_count_text_view)
        TextView forkCount;

        @BindView(R.id.star_count_text_view)
        TextView starCount;


        RepositoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(RepositoryDTO repository) {
            Picasso.with(itemView.getContext())
                    .load(repository.owner.avatar)
                    .placeholder(R.drawable.ic_owner)
                    .error(R.drawable.ic_owner)
                    .into(ownerImage);

            ownerName.setText(repository.owner.login);
            repositoryName.setText(repository.name);
            repositoryDescription.setText(repository.description);
            forkCount.setText(String.valueOf(repository.forks));
            starCount.setText(String.valueOf(repository.stars));
        }
    }

}
