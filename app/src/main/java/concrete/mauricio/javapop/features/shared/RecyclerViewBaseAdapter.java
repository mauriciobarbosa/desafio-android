package concrete.mauricio.javapop.features.shared;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 20/02/17.
 */
public abstract class RecyclerViewBaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected List<T> mItems;

    protected RecyclerViewBaseAdapter() {
        this.mItems = new ArrayList<>(0);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void setItems(@NonNull List<T> items) {
        checkNotNull(items);
        if (mItems == null)
            mItems = new ArrayList<>(items.size());
        int start = mItems.size() - 1;
        mItems.addAll(items);
        notifyItemRangeInserted(start, items.size());
    }

    public void clear() {
        if (mItems != null)
            mItems.clear();
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return mItems;
    }

}
