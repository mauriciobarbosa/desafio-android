package concrete.mauricio.javapop.features.shared;

import android.support.annotation.NonNull;

/**
 * Created by mauricio on 29/03/17.
 */
public interface BaseView<P extends BasePresenter> {

    void init();

    void setPresenter(@NonNull P presenter);

    boolean isActive();

}
