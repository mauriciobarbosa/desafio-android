package concrete.mauricio.javapop.data.source.remote;

import java.util.List;
import java.util.Map;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by mauricio on 29/03/17.
 */
interface GithubAPI {

    @GET("search/repositories")
    Observable<RepositoryPageDTO> getRepositories(@QueryMap Map<String, String> options);

    @GET("/repos/{owner}/{repo}/pulls")
    Observable<List<PullRequestDTO>> getPullRequests(@Path("owner") String owner, @Path("repo") String repo);

}
