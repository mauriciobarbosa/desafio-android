package concrete.mauricio.javapop.detail;

import com.annimon.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.channels.NotYetConnectedException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import concrete.mauricio.javapop.DataUtils;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.source.GitRepository;
import concrete.mauricio.javapop.features.repositorydetail.DetailRepositoryContract;
import concrete.mauricio.javapop.features.repositorydetail.PullRequestPresenter;
import concrete.mauricio.javapop.utils.schedulers.ImmediateSchedulerProvider;
import rx.Observable;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mauricio on 01/04/17.
 */
public class PullRequestPresenterTest {

    @Mock
    private AppManager mApp;

    @Mock
    private DetailRepositoryContract.View mView;

    @Mock
    private GitRepository mRepository;

    private PullRequestPresenter mPresenter;

    private List<PullRequestDTO> mPullRequests;

    private RepositoryDTO mRepo;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        when(mApp.getLanguage()).thenReturn("Java");
        when(mApp.sortBy()).thenReturn("stars");

        mPresenter = new PullRequestPresenter(mRepository, mView, new ImmediateSchedulerProvider());

        // O presenter não irá atualizar a view se ela não estiver ativa.
        when(mView.isActive()).thenReturn(true);

        mRepo = DataUtils.getRepository();
        mPullRequests = DataUtils.getPullRequests();

        when(mView.getRepository()).thenReturn(mRepo);

    }

    @Test
    public void loadPullRequests_showPullRequests() {

        when(mRepository.loadPullRequests(any())).thenReturn(Observable.just(mPullRequests));

        mPresenter.loadPullRequests();

        verify(mView).getRepository();

        verify(mRepository).loadPullRequests(eq(mRepo));

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();

        ArgumentCaptor<List> showReposArgumentCaptor = ArgumentCaptor.forClass(List.class);

        //noinspection unchecked
        inOrder.verify(mView).showPullRequests(showReposArgumentCaptor.capture());
        inOrder.verify(mView).hiddenLoadingIndicator();

        assertTrue(showReposArgumentCaptor.getValue().size() == mPullRequests.size());

    }

    @Test
    public void loadPullRequests_showNoPullRequests() {

        when(mRepository.loadPullRequests(any())).thenReturn(Observable.just(Collections.emptyList()));

        mPresenter.loadPullRequests();

        verify(mView).getRepository();

        verify(mRepository).loadPullRequests(eq(mRepo));

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();
        inOrder.verify(mView).showNoPullRequests();
        inOrder.verify(mView).hiddenLoadingIndicator();

    }

    @Test
    public void loadPullRequests_showNoConnection() {

        when(mRepository.loadPullRequests(any())).thenReturn(
                Observable.just(mPullRequests).doOnRequest(l -> {
                    throw new NotYetConnectedException();
                }));

        mPresenter.loadPullRequests();

        verify(mView).getRepository();

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();
        inOrder.verify(mView).showNoInternetConnection();
        inOrder.verify(mView).hiddenLoadingIndicator();

    }

    @Test
    public void loadPullRequests_getStatistics() {

        Map<Integer, Integer> statistics = mPresenter.getStatistics(mPullRequests);

        assertTrue(statistics.containsKey(DetailRepositoryContract.PULL_REQUEST_OPENED));
        assertTrue(statistics.containsKey(DetailRepositoryContract.PULL_REQUEST_CLOSED));

        int opened = (int) Stream.of(mPullRequests)
                .filter(pr -> pr.state.equals(PullRequestDTO.STATE_OPEN))
                .count();
        int closed = mPullRequests.size() - opened;

        assertTrue(statistics.get(DetailRepositoryContract.PULL_REQUEST_OPENED) == opened);
        assertTrue(statistics.get(DetailRepositoryContract.PULL_REQUEST_CLOSED) == closed);

    }

    @Test
    public void refreshPullRequests() {

        when(mRepository.refreshPullRequests(any())).thenReturn(Observable.just(mPullRequests));

        mPresenter.refreshPullRequests();

        verify(mView).getRepository();

        verify(mRepository).refreshPullRequests(eq(mRepo));

        InOrder inOrder = inOrder(mView);
        inOrder.verify(mView).showLoadingIndicator();

        ArgumentCaptor<List> showReposArgumentCaptor = ArgumentCaptor.forClass(List.class);

        //noinspection unchecked
        inOrder.verify(mView).showPullRequests(showReposArgumentCaptor.capture());
        inOrder.verify(mView).hiddenLoadingIndicator();

        assertTrue(showReposArgumentCaptor.getValue().size() == mPullRequests.size());

    }

}
