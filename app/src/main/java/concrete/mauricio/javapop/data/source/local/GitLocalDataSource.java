package concrete.mauricio.javapop.data.source.local;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import concrete.mauricio.javapop.data.PullRequestDTO;
import concrete.mauricio.javapop.data.RepositoryDTO;
import concrete.mauricio.javapop.data.RepositoryPageDTO;
import concrete.mauricio.javapop.data.source.GitDataSource;
import rx.Observable;

import static concrete.mauricio.javapop.utils.Preconditions.checkNotNull;

/**
 * Created by mauricio on 29/03/17.
 */
@Singleton
public class GitLocalDataSource implements GitDataSource {

    @Inject
    public GitLocalDataSource(@NonNull Context context) {
        checkNotNull(context);
        // TODO create dbHelper
    }

    @Override
    public Observable<RepositoryPageDTO> loadRepositories(int page) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Observable<RepositoryPageDTO> refreshRepositories() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Observable<List<PullRequestDTO>> loadPullRequests(@NonNull RepositoryDTO repository) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Observable<List<PullRequestDTO>> refreshPullRequests(@NonNull RepositoryDTO repository) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Observable<RepositoryPageDTO> reloadRepositories() {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
