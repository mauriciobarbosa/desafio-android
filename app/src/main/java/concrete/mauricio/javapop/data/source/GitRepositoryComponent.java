package concrete.mauricio.javapop.data.source;

import javax.inject.Singleton;

import concrete.mauricio.javapop.app.AppModule;
import dagger.Component;

/**
 * Created by mauricio on 29/03/17.
 */
@Singleton
@Component(modules = {GitRepositoryModule.class, AppModule.class})
public interface GitRepositoryComponent {

    GitRepository getGitRepository();

}
