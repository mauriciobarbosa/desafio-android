package concrete.mauricio.javapop.features.repositories;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.ActivityOptionsCompat;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import concrete.mauricio.javapop.R;
import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.features.repositorydetail.DetailActivity;
import concrete.mauricio.javapop.features.shared.BaseActivity;
import concrete.mauricio.javapop.utils.AppUtils;
import concrete.mauricio.javapop.utils.EspressoIdlingResource;

public class MainActivity extends BaseActivity implements BaseActivity.Callback {

    @Inject
    protected RepositoryPresenter mPresenter;

    /**
     * Propriedade para evitar double click em itens da lista.
     */
    private boolean disableTemporally;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.actv_main);

        AppManager app = (AppManager) getApplication();
        RepositoryFragment fragRepositories =
                (RepositoryFragment) getSupportFragmentManager().findFragmentById(R.id.frag_repositories);

        DaggerRepositoryComponent.builder()
                .gitRepositoryComponent(app.getRepositoryComponent())
                .repositoryPresenterModule(new RepositoryPresenterModule(fragRepositories))
                .build()
                .inject(this);

        fragRepositories.setPresenter(mPresenter);

    }

    @Override
    public void onItemSelected(Serializable item, ActivityOptionsCompat options) {
        if (!disableTemporally) {
            disableTemporally = true;
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.EXTRA_REPOSITORY, item);
            AppUtils.goToNextActivity(this, intent, options);
            new Handler().postDelayed(() -> disableTemporally = false, TimeUnit.SECONDS.toMillis(1));
        }
    }

    @Override
    public void setSubtitle(@NonNull String subtitle) {
        // do nothing
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

}
