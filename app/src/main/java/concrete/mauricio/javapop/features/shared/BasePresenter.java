package concrete.mauricio.javapop.features.shared;

/**
 * Created by mauricio on 29/03/17.
 */
public interface BasePresenter {

    void start();

}
