package concrete.mauricio.javapop.data.source;

import android.content.Context;

import javax.inject.Singleton;

import concrete.mauricio.javapop.app.AppManager;
import concrete.mauricio.javapop.data.source.local.GitLocalDataSource;
import concrete.mauricio.javapop.data.source.remote.GitRemoteDataSource;
import dagger.Module;
import dagger.Provides;

/**
 * Created by mauricio on 28/03/17.
 */
@Module
class GitRepositoryModule {

    @Singleton
    @Provides
    @Local
    GitDataSource provideLocalDataSource(Context context) {
        return new GitLocalDataSource(context);
    }

    @Singleton
    @Provides
    @Remote
    GitDataSource provideRemoteDataSource(AppManager appManager) {
        return new GitRemoteDataSource(appManager);
    }

}
